package com.rasyad.pointtransaksi.controllers;

import com.rasyad.pointtransaksi.Repositories.NasabahRepository;
import com.rasyad.pointtransaksi.Repositories.TransaksiRepository;
import com.rasyad.pointtransaksi.dto.NasabahDTO;
import com.rasyad.pointtransaksi.dto.TransaksiDTO;
import com.rasyad.pointtransaksi.dto.request.CetakBukuTabunganRequest;
import com.rasyad.pointtransaksi.dto.request.CreateNasabahRequest;
import com.rasyad.pointtransaksi.dto.request.CreateTransaksiRequest;
import com.rasyad.pointtransaksi.dto.response.GetTransaksiByRangeResponse;
import com.rasyad.pointtransaksi.models.Nasabah;
import com.rasyad.pointtransaksi.models.Transaksi;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/transaksi")
public class TransaksiController {

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private NasabahRepository nasabahRepository;
    @Autowired
    private TransaksiRepository transaksiRepository;

    // get all Nasabah
    @GetMapping("/get-all")
    public ResponseEntity<Object> readAllTransaksi(){

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;

        List<Transaksi> listTransaksiEntity = transaksiRepository.findAll();
        List<TransaksiDTO> listTransaksiDTO = new ArrayList<>();

        if(listTransaksiEntity.isEmpty()) {
            result.put("Data", "Nasabah list is empty");
            responseStatus = HttpStatus.BAD_REQUEST;
        }
        else {

            for (Transaksi item : listTransaksiEntity) {

                TransaksiDTO TransaksiDTO = modelMapper.map(item, TransaksiDTO.class);

                listTransaksiDTO.add(TransaksiDTO);

            }

            result.put("Message", "Read all Transaksi success");
            result.put("Data", listTransaksiDTO);
            result.put("Total", listTransaksiDTO.size());
        }

        result.put("Status", responseStatus);
        return new ResponseEntity<Object>(result, responseStatus);

    }

    // get all Nasabah by range
    @GetMapping("/get-all-range")
    public ResponseEntity<Object> readAllTransaksiRange(@Valid @RequestBody CetakBukuTabunganRequest body){

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;

        List<Transaksi> listTransaksiEntity = transaksiRepository.findTransaksiByDateRange(body.getAccountId(), body.getStartDate(), body.getEndDate());
        List<GetTransaksiByRangeResponse> listTransaksiDTO = new ArrayList<>();

        if(listTransaksiEntity.isEmpty()) {
            result.put("Data", "Nasabah list is empty");
            responseStatus = HttpStatus.BAD_REQUEST;
        }
        else {

            for (Transaksi item : listTransaksiEntity) {

                GetTransaksiByRangeResponse response = new GetTransaksiByRangeResponse();

                response.setTransactionDate(item.getTransactionDate());
                response.setDescription(item.getDescription());
                response.setBalance(item.getBalance());

                if(item.getDebitCreditStatus().equalsIgnoreCase("c")){
                    response.setCredit(item.getAmount());
                    response.setDebit(0.0);
                }
                else if(item.getDebitCreditStatus().equalsIgnoreCase("d")){
                    response.setDebit(item.getAmount());
                    response.setCredit(0.0);
                }

                listTransaksiDTO.add(response);

            }

            result.put("Message", "Read all Transaksi success");
            result.put("Data", listTransaksiDTO);
            result.put("Total", listTransaksiDTO.size());
        }

        result.put("Status", responseStatus);
        return new ResponseEntity<Object>(result, responseStatus);

    }

    // create a new Transaksi
    @PostMapping("/create")
    public ResponseEntity<Object> createNasabah(@Valid @RequestBody CreateTransaksiRequest body){

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;

        Nasabah nasabah= nasabahRepository.findById(body.getAccountId()).get();

        Transaksi transaksi = new Transaksi();

        transaksi.setNasabah(nasabah);
        transaksi.setAmount(body.getAmount());
        transaksi.setDescription(body.getDescription());
        transaksi.setTransactionDate(body.getTransactionDate());
        transaksi.setDebitCreditStatus(body.getDebitCreditStatus());


        double balance = nasabah.getBalance();
        double amount = body.getAmount();
        Integer point = nasabah.getPoint();

        if(body.getDebitCreditStatus().equalsIgnoreCase("c")){
            balance = balance + body.getAmount();
        } else if (body.getDebitCreditStatus().equalsIgnoreCase("d")) {
            balance = balance - body.getAmount();

            if (body.getDescription().equalsIgnoreCase("beli pulsa")){

                double requirement1 = 10000;
                double requirement2 = 20000;

                if (amount >= requirement1){
                    amount = amount - requirement1;
                    System.out.println("============= Amount 1 : " + amount);
                }

                if(amount >= requirement2){
                    amount = amount - requirement2;
                    point = point + ((int)requirement2 / 1000);
                    System.out.println("======== Point 1 : " + point);
                    System.out.println("============= Amount 2 : " + amount);
                }
                else{
                    point = point + (int)((amount / 1000));
                    amount = 0;
                }


                if(amount > 0){
                    point = point + (int)((amount / 1000) * 2);
                    System.out.println("======== Point 2 : " + point);
                }

            }
            else if (body.getDescription().equalsIgnoreCase("bayar listrik")){

                double requirement1 = 50000;

                if (amount >= requirement1){
                    amount = amount - requirement1;
                    System.out.println("============= Amount 1 : " + amount);
                }


                if(amount >= requirement1){
                    amount = amount - requirement1;
                    point = point + ((int)requirement1 / 2000);
                    System.out.println("======== Point 1 : " + point);
                    System.out.println("============= Amount 2 : " + amount);
                }
                else{

                    point = point + (int)((amount / 2000));
                    amount = 0;

                }


                if(amount > 0){
                    point = point + (int)((amount / 2000) * 2);
                    System.out.println("======== Point 2 : " + point);
                }

            }

        }

        transaksi.setBalance(balance);
        nasabah.setBalance(balance);
        nasabah.setPoint(point);

        transaksiRepository.save(transaksi);
        nasabahRepository.save(nasabah);

        TransaksiDTO transaksiDTOa = modelMapper.map(body, TransaksiDTO.class);

        result.put("Data", body);
        result.put("Message", "Nasabah has been created");
        result.put("Status", responseStatus);

        return new ResponseEntity<Object>(result, responseStatus);

    }

}
