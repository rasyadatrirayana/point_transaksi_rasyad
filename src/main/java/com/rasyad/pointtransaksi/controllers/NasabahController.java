package com.rasyad.pointtransaksi.controllers;

import com.rasyad.pointtransaksi.Repositories.NasabahRepository;
import com.rasyad.pointtransaksi.dto.NasabahDTO;
import com.rasyad.pointtransaksi.dto.request.CreateNasabahRequest;
import com.rasyad.pointtransaksi.dto.response.GetAllNasabahPointResponse;
import com.rasyad.pointtransaksi.dto.response.GetAllNasabahResponse;
import com.rasyad.pointtransaksi.models.Nasabah;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/nasabah")
public class NasabahController {

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private NasabahRepository nasabahRepository;

    // get all Nasabah
    @GetMapping("/get-all")
    public ResponseEntity<Object> readAllNasabah(){

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;

        List<Nasabah> listNasabahEntity = nasabahRepository.findAll();
        List<GetAllNasabahResponse> listNasabahDTO = new ArrayList<>();

        if(listNasabahEntity.isEmpty()) {
            result.put("Data", "Nasabah list is empty");
            responseStatus = HttpStatus.BAD_REQUEST;
        }
        else {

            for (Nasabah item : listNasabahEntity) {

                GetAllNasabahResponse NasabahDTO = new GetAllNasabahResponse(item.getAccountId(), item.getName());

                listNasabahDTO.add(NasabahDTO);

            }

            result.put("Message", "Read all Nasabah success");
            result.put("Data", listNasabahDTO);
            result.put("Total", listNasabahDTO.size());
        }

        result.put("Status", responseStatus);
        return new ResponseEntity<Object>(result, responseStatus);

    }

    // get all Nasabah point
    @GetMapping("/get-all-point")
    public ResponseEntity<Object> readAllNasabahPoint(){

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;

        List<Nasabah> listNasabahEntity = nasabahRepository.findAll();
        List<GetAllNasabahPointResponse> listNasabahDTO = new ArrayList<>();

        if(listNasabahEntity.isEmpty()) {
            result.put("Data", "Nasabah list is empty");
            responseStatus = HttpStatus.BAD_REQUEST;
        }
        else {

            for (Nasabah item : listNasabahEntity) {

                GetAllNasabahPointResponse NasabahDTO = new GetAllNasabahPointResponse(item.getAccountId(), item.getName(), item.getPoint());

                listNasabahDTO.add(NasabahDTO);

            }

            result.put("Message", "Read all Nasabah success");
            result.put("Data", listNasabahDTO);
            result.put("Total", listNasabahDTO.size());
        }

        result.put("Status", responseStatus);
        return new ResponseEntity<Object>(result, responseStatus);

    }

    // create a new Nasabah
    @PostMapping("/create")
    public ResponseEntity<Object> createNasabah(@Valid @RequestBody CreateNasabahRequest body){

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;

        Nasabah nasabah = modelMapper.map(body, Nasabah.class);
        nasabah.setPoint(0);

        nasabahRepository.save(nasabah);

        NasabahDTO NasabahDTO = modelMapper.map(body, NasabahDTO.class);

        result.put("Data", body);
        result.put("Message", "Nasabah has been created");
        result.put("Status", responseStatus);

        return new ResponseEntity<Object>(result, responseStatus);

    }

    // update an Nasabah
    @PutMapping("/update/{DTO}")
    public ResponseEntity<Object> updateNasabah(@PathVariable(name = "DTO") Integer Id, @Valid @RequestBody Nasabah NasabahDetails) {

        Map<String, Object> result = new HashMap<>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";
        NasabahDetails.setAccountId(Id);

        if (nasabahRepository.existsById(Id)){
            message = "Nasabah with the DTO of " + Id + " has been updated";

            Nasabah Nasabah = nasabahRepository.findById(Id).get();

            Nasabah = modelMapper.map(NasabahDetails, Nasabah.class);

            nasabahRepository.save(Nasabah);
        }
        else {
            message = "Couldn't found Nasabah with the DTO of : " + Id;
            responseStatus = HttpStatus.NOT_FOUND;
        }

        result.put("Message", message);
        result.put("Status", responseStatus);

        return new ResponseEntity<Object>(result, responseStatus);

    }

    // delete a Nasabah
    @DeleteMapping("/delete/{DTO}")
    public ResponseEntity<Object> deleteNasabah(@PathVariable(name = "DTO") Integer Id){

        Map<String, Object> result = new HashMap<String, Object>();
        HttpStatus responseStatus = HttpStatus.OK;
        String message = "";

        Nasabah Nasabah = nasabahRepository.findById(Id).get();

        if (nasabahRepository.existsById(Id)){
            NasabahDTO NasabahDTO = modelMapper.map(Nasabah, NasabahDTO.class);
            result.put("Data", NasabahDTO);
            message = "Nasabah with the DTO of " + Id + " has been deleted";

            nasabahRepository.delete(Nasabah);
        }
        else {
            message = "Couldn't found Nasabah with the DTO of : " + Id;
            responseStatus = HttpStatus.NOT_FOUND;
        }

        result.put("Message", message);
        result.put("Status", responseStatus);

        return new ResponseEntity<Object>(result, responseStatus);

    }


}
