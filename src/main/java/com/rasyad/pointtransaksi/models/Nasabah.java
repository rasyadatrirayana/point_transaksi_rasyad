package com.rasyad.pointtransaksi.models;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Table(name = "nasabah")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Nasabah {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nasabah_sequence")
    @SequenceGenerator(name = "nasabah_sequence", sequenceName = "nasabah_account_id_seq", allocationSize = 1)
    @Column(name = "account_id")
    private Integer accountId;

    @Column(name = "name")
    private String name;

    @Column(name = "point")
    private Integer point;

    @Column(name = "balance")
    private Double balance;

    @OneToMany(mappedBy = "nasabah", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private List<Transaksi> transaksiList;

}
