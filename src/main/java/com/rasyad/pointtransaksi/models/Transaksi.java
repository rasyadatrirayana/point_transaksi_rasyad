package com.rasyad.pointtransaksi.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "transaksi", schema = "public")
@Data
public class Transaksi {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaksi_sequence")
    @SequenceGenerator(name = "transaksi_sequence", sequenceName = "transaksi_transaction_id_seq", allocationSize = 1)
    @Column(name = "transaction_id")
    private Integer transactionId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id")
    private Nasabah nasabah;

    @Column(name = "transaction_date")
    private Date transactionDate;

    @Column(name = "description")
    private String description;

    @Column(name = "debit_credit_status")
    private String debitCreditStatus;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "balance")
    private Double balance;

}
