package com.rasyad.pointtransaksi.Repositories;

import com.rasyad.pointtransaksi.models.Nasabah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NasabahRepository extends JpaRepository<Nasabah, Integer> {

    @Query(nativeQuery = true, value =
            "SELECT * " +
                    "FROM employee Emp " +
                    "WHERE LOWER(Emp.name) ILIKE CONCAT('%', :name, '%')")
    List<Nasabah> findNasabahByName(@Param("name") String name);


}
