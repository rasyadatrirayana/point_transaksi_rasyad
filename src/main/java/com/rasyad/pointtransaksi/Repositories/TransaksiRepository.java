package com.rasyad.pointtransaksi.Repositories;

import com.rasyad.pointtransaksi.models.Nasabah;
import com.rasyad.pointtransaksi.models.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface TransaksiRepository extends JpaRepository<Transaksi, Integer> {

    @Query(nativeQuery = true, value =
            "SELECT tr.*  " +
            "FROM transaksi tr " +
            "WHERE tr.account_id = :accountId " +
            "AND tr.transaction_date BETWEEN :startDate AND :endDate")
    List<Transaksi> findTransaksiByDateRange(@Param("accountId") Integer accountId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

}
