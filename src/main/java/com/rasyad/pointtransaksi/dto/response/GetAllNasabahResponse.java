package com.rasyad.pointtransaksi.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetAllNasabahResponse {
    private Integer accountId;
    private String name;
}
