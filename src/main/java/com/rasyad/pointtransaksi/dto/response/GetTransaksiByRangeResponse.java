package com.rasyad.pointtransaksi.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetTransaksiByRangeResponse {
    private Date transactionDate;
    private String description;
    private Double credit;
    private Double debit;
    private Double balance;
}
