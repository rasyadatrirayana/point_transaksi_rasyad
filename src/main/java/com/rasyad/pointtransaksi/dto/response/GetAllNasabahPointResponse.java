package com.rasyad.pointtransaksi.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetAllNasabahPointResponse {
    private Integer accountId;
    private String name;
    private Integer point;
}
