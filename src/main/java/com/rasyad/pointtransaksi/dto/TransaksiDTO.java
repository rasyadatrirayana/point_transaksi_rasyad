package com.rasyad.pointtransaksi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransaksiDTO {

    private Integer transactionId;
    private Integer accountId;
    private Date transactionDate;
    private String description;
    private String debitCreditStatus;
    private Double amount;
    private Double balance;

}
