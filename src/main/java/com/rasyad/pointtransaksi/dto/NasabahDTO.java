package com.rasyad.pointtransaksi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NasabahDTO {

    private Integer accountId;
    private String name;
    private Integer point;
    private Double balance;

}
