package com.rasyad.pointtransaksi.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateNasabahRequest {
    private Integer accountId;
    private String name;
    private Integer point;
    private Double balance;
}
