package com.rasyad.pointtransaksi.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CetakBukuTabunganRequest {
    private Integer accountId;
    private Date startDate;
    private Date endDate;
}
