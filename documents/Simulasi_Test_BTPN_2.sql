/*
 Navicat Premium Data Transfer

 Source Server         : local_db
 Source Server Type    : PostgreSQL
 Source Server Version : 140004
 Source Host           : localhost:5432
 Source Catalog        : Simulasi_Test_BTPN_2
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140004
 File Encoding         : 65001

 Date: 23/08/2023 13:08:00
*/


-- ----------------------------
-- Sequence structure for Nasabah_AccountId_Seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Nasabah_AccountId_Seq";
CREATE SEQUENCE "public"."Nasabah_AccountId_Seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for Transaksi_TransactionId_Seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Transaksi_TransactionId_Seq";
CREATE SEQUENCE "public"."Transaksi_TransactionId_Seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for nasabah
-- ----------------------------
DROP TABLE IF EXISTS "public"."nasabah";
CREATE TABLE "public"."nasabah" (
  "account_id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "point" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of nasabah
-- ----------------------------

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS "public"."transaksi";
CREATE TABLE "public"."transaksi" (
  "account_id" int4 NOT NULL,
  "transaction_date" date,
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "debit_credit_status" varchar(255) COLLATE "pg_catalog"."default",
  "amount" money,
  "transaction_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of transaksi
-- ----------------------------

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."Nasabah_AccountId_Seq"
OWNED BY "public"."nasabah"."account_id";
SELECT setval('"public"."Nasabah_AccountId_Seq"', 1, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."Transaksi_TransactionId_Seq"
OWNED BY "public"."transaksi"."transaction_id";
SELECT setval('"public"."Transaksi_TransactionId_Seq"', 1, false);

-- ----------------------------
-- Primary Key structure for table nasabah
-- ----------------------------
ALTER TABLE "public"."nasabah" ADD CONSTRAINT "nasabah_pkey" PRIMARY KEY ("account_id");

-- ----------------------------
-- Primary Key structure for table transaksi
-- ----------------------------
ALTER TABLE "public"."transaksi" ADD CONSTRAINT "Transaksi_pkey" PRIMARY KEY ("transaction_id");

-- ----------------------------
-- Foreign Keys structure for table transaksi
-- ----------------------------
ALTER TABLE "public"."transaksi" ADD CONSTRAINT "acc_id_fk" FOREIGN KEY ("account_id") REFERENCES "public"."nasabah" ("account_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
